<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Auth\GoogleController;
use App\Http\Controllers\Auth\FacebookController;
use App\Http\Controllers\CategoryController;
// use App\Http\Controllers\SubcategoryController;

use Laravel\Socialite\Facades\Socialite;




// use Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('auth/google', [App\Http\Controllers\Auth\GoogleController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [App\Http\Controllers\Auth\GoogleController::class, 'handleGoogleCallback']);

Route::get('auth/facebook', [App\Http\Controllers\Auth\FacebookController::class, 'redirectToFacebook']);
Route::get('auth/facebook/callback', [App\Http\Controllers\Auth\FacebookController::class, 'handleFacebookCallback']);

// Route::get('/category', [CategoryController::class, 'display']);
// Route::get('/category', [SubcategoryController::class, 'displayy']);


// Route::POST('/store', [CategoryController::class, 'insert']);
// Route::POST('/substore', [SubcategoryController::class, 'insertt']);

Route::get('/category/add', [CategoryController::class, 'create'])->name('category.create');
Route::post('/category/add', [CategoryController::class, 'store'])->name('category.store');
