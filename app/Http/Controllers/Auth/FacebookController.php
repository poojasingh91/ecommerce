<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Auth;
use Exception;
use App\Models\User;

class FacebookController extends Controller
{
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }


    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            // error_log
            $finduser = User::where('facebook_id', $user->id)->first();
            if ($finduser) {

                Auth::login($finduser);

                return redirect('/home');
            } else {
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'facebook_id' => $user->id,
                    'password' => encrypt('poojA123')
                ]);

                Auth::login($newUser);

                return redirect('/home');
            }
        } catch (Exception $e) {
            // dd($e->getMessage());
            error_log($e);
            return redirect('/home');
        }

        // return redirect()->route('home');
        // try {
        //     $user = Socialite::driver('facebook')->user();

        //     $saveUser = User::updateOrCreate(
        //         [
        //             'facebook_id' => $user->getId(),
        //         ],
        //         [
        //             'name' => $user->getName(),
        //             'email' => $user->getEmail(),
        //             'password' => Hash::make($user->getName() . '@' . $user->getId())

        //         ]
        //     );
        //     Auth::loginUsingId($saveUser->id);

        //     return redirect()->route('home');
        // } catch (\Throwable $th) {
        //     throw $th;
        // }
    }

    // public function handleFacebookCallback()
    // {
    //     try {
    //         $user =Socialite::driver('facebook')->user();
    //         $finduser = User::where('facebook_id',$user->id)->first();

    //         if($finduser)
    //         {
    //             Auth::login($finduser);
    //             return redirect('/home');
    //         }
    //         else {
    //             $newUser = User::create([
    //                 'name'=> $user->name,
    //                 'email' => $user->email,
    //                 'google_id' => $user->id,
    //                 'password' => encrypt('123456'),
    //                 ]);
    //                 Auth::login($newuser);
    //                 return redirect('/home');
    //         }
    //     } catch (Exception $e) {
    //         dd($e->getMessage());
    //     }
    // }
}
